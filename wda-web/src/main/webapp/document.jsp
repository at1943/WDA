<%@page import="com.farm.wda.util.AppConfig"%>
<%@page import="java.io.File"%>
<%@page import="com.farm.wda.Beanfactory"%>
<%@ page language="java" pageEncoding="utf-8"%>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><%=AppConfig.getString("config.web.title")%></title>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body style="background-color: #8a8a8a;">
	<jsp:include page="/commons/head.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<img alt="type" style="margin: auto;" class="img-responsive"
							src="img/type.png">
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">配置文件docTypeConf.xml</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<img src="img/xml.png" alt="xml" />
							</div>
							<div class="col-md-6">
								<ul>
									<li>conf/files:被转换为的文件类型 exname为支持的文件类型名称 filename为生成文件名</li>
									<li>filename的path参数为文件相对路径</li>
									<li>conf/types:文件转换关系对照</li>
									<li>conf/types/name:源文件后缀名</li>
									<li>conf/types/target:目标文件类型，可以生成多种,需要与conf/files/file/exname一致/</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">配置文件config.properties</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<img src="img/conf.png" style="width: 100%" alt="xml" />
							</div>
							<div class="col-md-6">
								<ul>
									<li>config.file.dir.path:文件存储地址，需要配置到webroot下</li>
									<li>config.server.openoffice.cmd:openoffice的soffice服务启动命令</li>
									<li>config.rmi.port:rmi绑定端口</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">编码</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<img src="img/demo.png" style="width: 100%" alt="xml" />
							</div>
							<div class="col-md-12">
								<pre>/**
*开始生产WEB文档
generateDoc(Stringkey,Filefile)

/**
*开始生产WEB文档
generateDoc(Stringkey,Filefile,StringfileTypeName)

/**
*文档是否已经生成完毕
isGenerated(Stringkey,Stringdoctype)

/**
*文档是否有日志记录
isLoged(Stringkey);

/**
*获得日志地址
getlogURL(Stringkey);

/**
*获得文档文本字符串
getText(Stringkey)

/**
*获得在线文档浏览的URL
getUrl(Stringkey,StringdocType)</pre>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">RMI调用</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
							 <pre>WdaAppInter personService = (WdaAppInter) Naming.lookup("rmi://127.0.0.1:8888/wda");
personService.generateDoc("1234", new File("D:\\doc\\1.docx"));</pre>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</body>
</html>