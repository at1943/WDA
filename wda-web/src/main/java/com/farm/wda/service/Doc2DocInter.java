package com.farm.wda.service;

import java.io.File;

/**
 * 文档转换工具
 * 
 * @author Administrator
 *
 */
public interface Doc2DocInter {

	/**
	 * 转换office文档
	 * 
	 * @param docFile
	 *            原文档
	 * @param tarFile
	 *            目标文档
	 * @return
	 */
	public File convert(File docFile, File tarFile);

	/**
	 * 转换office文档
	 * 
	 * @param docFile
	 *            原文件
	 * @param typename
	 *            源文件类型 doc、docx
	 * @param tarFile
	 *            目标文件
	 * @return
	 */
	public File convert(File docFile, String typename, File tarFile);

}
